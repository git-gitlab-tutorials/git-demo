# Notepad++ and MarkDownViewerPlusPlus plugin
Notepad++ can be used to edit the markdown files which are easier to display on gitlab webpages.

# Install Notepad++ and MarkDownViewerPlusPlus
- https://notepad-plus-plus.org/
- Open notepad++, on pull-down menu -> `Plugins` -> `Plugins Admin` -> search `markdownviewer++` -> click install button -> done

# How to use:
- Create file by filename.md (*.md stands for markdown file)
- syntax: https://docs.gitlab.com/ee/user/markdown.html