# git demo (My learning history for using git~.~)

## List of Contents
- ![Git Installation tips](git_general_setup.md)
- ![Git Self-Learning Websites (Lang = Traditional Chinese)](https://gitbook.tw/chapters/introduction/what-is-git.html)


## frequently used git command shortcut to save my life >.<
- check all commit history:
```
git lola    :=  git log --graph --decorate --pretty=oneline --abbrev-commit --all
```
- check only last 10 commit history: (useful when the history is long)
```
git lol     :=  git log --graph --decorate --pretty=oneline -n 10 --abbrev-commit
```

- basic git commands but extremely frequently used
```
git st  :=  git status
git co  :=  git checkout
git aa  :=  git add . or git add -A
git ci  :=  git commit (then edit the commit message in the opened text editor)
git cm "commit message" :=  git commit -m "commit message"
git br  :=  git branch 
```


## adding these commands into the shortcut
These shortcut-adding commands are usually used once after a fresh install of 
git on a new workstation.
You can copy these commands below and place on the git bash. Done~ Enjoy!
```
git config --global alias.lola "log --graph --decorate --pretty=oneline --abbrev-commit --all"
git config --global alias.lol "log --graph --decorate --pretty=oneline -n 10 --abbrev-commit"
git config --global alias.st "status"
git config --global alias.co "checkout"
git config --global alias.sw "switch"
git config --global alias.aa "add -A"
git config --global alias.ci "commit"
git config --global alias.cm "commit -m"
git config --global alias.br "branch"


```

## how to use the shortcut in daily life:
- before starting to work / change anything
```
git st                  # double check wnything missing to track last time
git lol or git lola     # show current local commit history
git pull                # update from server, if using gitlab/github
git lol or git lola     # show updated commit history, if any new things downloaded via 'git pull'
now start doing your work~
```
- after working on a while you have new files which are going to be added and commited:
```

git st  # check which files have been edited
git aa  # usually add all files listed above, you may select your own files to be staged
git st  # check again for the staged files, sometimes the 1st "git st" may only list the dir without the files inside

git cm "your message"
git lol # show commit history after current commit

[optional]
git push
```
Or faster way:
```
git st
git acm "your message"

git push
git lol
```

## using Markdown file (like this file)
- good for displaying on gitlab webpage
- can use notepad++ with the markdownviewer++ plugin to draft offline 
  - ![details here](Notepad_PlusPlus_with_MarkDownViewerPlusPlus.md)
