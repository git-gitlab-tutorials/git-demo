# Git General Setup

## Installing Git and check out the settings below (OS = Windows 10):
- (Recommended) Install notepad-plus-plus from [here][notepad-plus-plus] for default editer
  - Easier to edit/preview markdown file (like this file) offline
- Windows Explorer integration (easier to start Git on your directory directly)
  - Git Bash Here
  - Git GUI here
- Check-out Windors-style, commit Unix-style line endings (portable from/to Unix machines)
- Install Git as is

## Some useful command setup
- If your files `contains chinese characters` in the file name/dir (e.g. chinese driver documents), \
To correctly display the path/name, execute the fillowing command:
```
git config --global core.quotepath false
```

## Setup user working on this repo
- Global user config
```
git config --global user.name "username"
git config --global user.email "user@gmail.com"
```
- Local user config, effective only on this repo, if this is not set, global will be used.
```
git config --local user.name "username"
git config --local user.email "user@gmail.com"
```
`P.S. if the windows-OS computer (e.g. lab computer) is shared with multiple users,\ 
please remove the credential login information before starting working:`

-> press `start menu` -> type `Credential Manager` -> Press 'Enter' to open \
-> Press `Windows Credentials` Tab -> locate `git:https://gitlab.com` in the `Generic Credentials`
-> Click the small Expand button and click `remove` -> click yes to confirm.\
-> Now you may work on your own repo, and you will be prompted to enter login information by the commands (e.g. `git pull` / `git push`)





[notepad-plus-plus]: https://notepad-plus-plus.org/ "download notepad++ here"